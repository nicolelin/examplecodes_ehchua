package Multithreading.ResponsiveUI;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

//The GUI program has two buttons. Pushing the "Start Counting" button starts the counting. Pushing the "Stop Counting" button is supposed to stop (pause) the counting. The two button-handlers communicate via a boolean flag called stop. The stop-button handler sets the stop flag; while the start-button handler checks if stop flag has been set before continuing the next count.

//http://www.ntu.edu.sg/home/ehchua/programming/java/J5e_multithreading.html

/**
 * Illustrate Unresponsive UI problem caused by "busy" Event-Dispatching Thread
 */
public class UnresponsiveUI extends JFrame {
    private boolean stop = false;  // start or stop the counter
    private JTextField tfCount;
    private int count = 1;

    /**
     * Constructor to setup the GUI components
     */
    public UnresponsiveUI() {
        Container cp = this.getContentPane();
        cp.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));
        cp.add(new JLabel("Counter"));
        tfCount = new JTextField(count + "", 10);
        tfCount.setEditable(false);
        cp.add(tfCount);

        JButton btnStart = new JButton("Start Counting");
        cp.add(btnStart);
        btnStart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                stop = false;
                for (int i = 0; i < 100000; ++i) {
                    if (stop) break;  // check if STOP button has been pushed,
                    //  which changes the stop flag to true
                    tfCount.setText(count + "");
                    ++count;
                }
            }
        });
        JButton btnStop = new JButton("Stop Counting");
        cp.add(btnStop);
        btnStop.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                stop = true;  // set the stop flag
            }
        });

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Counter");
        setSize(300, 120);
        setVisible(true);
    }

    /**
     * The entry main method
     */
    public static void main(String[] args) {
        // Run GUI codes in Event-Dispatching thread for thread safety
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new UnresponsiveUI();  // Let the constructor do the job
            }
        });
    }
}

//Observations

//        The main() method is started in the "main" thread.

//        The JRE's windowing subsystem, via SwingUtilities.invokeLater(), starts 3 threads: "AWT-Windows" (daemon thread), "AWT-Shutdown" and "AWT-EventQueue-0". The "AWT-EventQueue-0" is known as the Event-Dispatching Thread (EDT), which is the one and only thread responsible for handling all the events (such as clicking of buttons) and refreshing the display to ensure thread safety in GUI operations and manipulating GUI components. The constructor UnresponsiveUI() is scheduled to run on the Event-Dispatching thread (via invokeLater()), after all the existing events have been processed. The "main" thread exits after the main() method completes. A new thread called "DestroyJavaVM" is created.

//        When you click the START button, the actionPerformed() is run on the EDT. The EDT is now fully-occupied with the compute-intensive counting-loop. In other words, while the counting is taking place, the EDT is busy and unable to process any event (e.g., clicking the STOP button or the window-close button) and refresh the display - until the counting completes and EDT becomes available. As the result, the display freezes until the counting-loop completes.

//    The trace shows that:
//        The main() method starts in the "main" thread.
//        A new thread "AWT-Windows" (Daemon thread) is started when we step-into the constructor "new UnresponsiveUI()" (because of the "extends JFrame").
//        After executing "setVisible(true)", another two threads are created - "AWT-Shutdown" and "AWT-EventQueue-0" (i.e., the EDT).
//        The "main" thread exits after the main() method completes. A new thread called "DestroyJavaVM" is created.
//        At this point, there are 4 threads running - "AWT-Windows", "AWT-Shutdown" and "AWT-EventQueue-0 (EDT)" and "DestroyJavaVM".
//        Clicking the START button invokes the actionPerformed() in the EDT.
//        In the earlier case, the EDT is started via the invokeLater(); while in the later case, the EDT starts after setVisible().