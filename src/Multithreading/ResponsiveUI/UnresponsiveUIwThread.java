package Multithreading.ResponsiveUI;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/** Illustrate the Unresponsive UI problem caused by "starved" event-dispatching thread */
public class UnresponsiveUIwThread extends JFrame {
    private boolean stop = false;
    private JTextField tfCount;
    private int count = 1;

    /** Constructor to setup the GUI components */
    public UnresponsiveUIwThread() {
        Container cp = getContentPane();
        cp.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));
        cp.add(new JLabel("Counter"));
        tfCount = new JTextField(count + "", 10);
        tfCount.setEditable(false);
        cp.add(tfCount);

        JButton btnStart = new JButton("Start Counting");
        cp.add(btnStart);
        btnStart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                stop = false;
                // Create our own Thread to do the counting
                Thread t = new Thread() {
                    @Override
                    public void run() {  // override the run() to specify the running behavior
                        for (int i = 0; i < 100000; ++i) {
                            if (stop) break;
                            tfCount.setText(count + "");
                            ++count;
                        }
                    }
                };
                t.start();  // call back run()
            }
        });

        JButton btnStop = new JButton("Stop Counting");
        cp.add(btnStop);
        btnStop.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                stop = true;
            }
        });

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Counter");
        setSize(300, 120);
        setVisible(true);
    }

    /** The entry main method */
    public static void main(String[] args) {
        // Run GUI codes in Event-Dispatching thread for thread safety
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new UnresponsiveUIwThread();  // Let the constructor do the job
            }
        });
    }
}

//Observations

//A new thread is created by sub-classing the Thread class, with an anonymous inner class. We override the run() method to specify the running behavior of the thread, which performs the compute-intensive counting. An instance is created. Invoking the start() method of the instance causes the run() to execute on its own thread. (The details on creating new thread will be explained later.)
//        The responsiveness improves slightly. But the proper counter value is still not shown, and there is a delay in response to the "STOP" button. (You may not see the difference running with a dual-core processor.)
//        This is because the counting thread does not voluntarily yield control to the EDT. The "starved" EDT is unable to update the display and response to the "STOP" button. Nonetheless, the JVM may force the counting thread to yield control according to the scheduling algorithm, which results in delay on updating the display (not sure about this).

//        Tracing the Threads (Advanced)
//        When the "START" button is clicked, a new thread called "Thread-n" (n is a running number) is created to run the compute-intensive counting-loop. However, this thread is not programmed to yield control to other threads, in particular, the event-dispatching thread.
//        This program is, however, slightly better than the previous program. The display is updated, and the clicking of "STOP" button has its effect after some delays.