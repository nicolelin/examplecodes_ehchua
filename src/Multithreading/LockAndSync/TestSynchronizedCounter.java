package Multithreading.LockAndSync;

/** A monitor is an object that can be used to block and revive thread. It is supported in the java.lang.Object root class, via these mechanisms:
        A lock for each object.
        The keyword synchronized for accessing object's lock.
        The wait(), notify() and notifyAll() methods in java.lang.Object for controlling threads.
        Each Java object has a lock. At any time, the lock is controlled by, at most, a single thread. You could mark a method or a block of the codes with keyword synchronized. A thread that wants to execute an object's synchronized code must first attempt to acquire its lock. If the lock is under the control of another thread, then the attempting thread goes into the Seeking Lock state and becomes ready only when the lock becomes available. When a thread that owns a lock completes the synchronized code, it gives up the lock. **/

public class TestSynchronizedCounter {
    public static void main(String[] args) {
        Thread threadIncrement = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < 10; ++i) {
                    SynchronizedCounter.increment();
                    try {
                        sleep(1);
                    } catch (InterruptedException e) {}
                }
            }
        };

        Thread threadDecrement = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < 10; ++i) {
                    SynchronizedCounter.decrement();
                    try {
                        sleep(1);
                    } catch (InterruptedException e) {}
                }
            }
        };

        threadIncrement.start();
        threadDecrement.start();
    }
}