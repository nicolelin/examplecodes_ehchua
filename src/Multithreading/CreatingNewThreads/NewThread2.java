package Multithreading.CreatingNewThreads;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by ylin183 on 1/06/2017.
 */

// Implement Runnable Interface and use abstract method run()

//    Create a class that implements the Runnable interface and provide the implementation to the abstract method run() to specify the running behavior of the thread. Construct a new Thread instance using the constructor with a Runnable object and invoke the start() method, which will call back run() on a new thread.

public class NewThread2 extends JFrame {
    private boolean stop = false;  // start or stop the counter
    private JTextField tfCount;
    private int count = 1;

    /**
     * Constructor to setup the GUI components
     */
    public NewThread2() {
        Container cp = getContentPane();
        cp.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));
        cp.add(new JLabel("Counter"));
        tfCount = new JTextField(count + "", 10);
        tfCount.setEditable(false);
        cp.add(tfCount);

        JButton btnStart = new JButton("Start Counting");
        cp.add(btnStart);
        btnStart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                stop = false;

// Create a new Thread to do the counting

// Creating a new Thread
// Create an anonymous instance of an anonymous inner class that implements Runnable
// and use the instance as the argument of Thread's constructor.

                Thread t = new Thread(new Runnable() {
                    // Provide implementation to abstract method run() to specify the running behavior
                    @Override
                    public void run() {
                        for (int i = 0; i < 100000; ++i) {
                            if (stop) break;
                            tfCount.setText(count + "");
                            ++count;
                            // Suspend itself and yield control to other threads
                            // Also provide the necessary delay
                            try {
                                Thread.sleep(10);  // milliseconds
                            } catch (InterruptedException ex) {
                            }
                        }
                    }
                });
                t.start();  // call back run() in new thread
            }
        });

        JButton btnStop = new JButton("Stop Counting");
        cp.add(btnStop);
        btnStop.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                stop = true;
            }
        });

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Counter");
        setSize(300, 120);
        setVisible(true);
    }

    /**
     * The entry main method
     */

    public static void main(String[] args) {
        // Run GUI codes in Event-Dispatching thread for thread safety
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new NewThread1();  // Let the constructor do the job
            }
        });
    }
}

//Observations

//    The second method is needed as Java does not support multiple inheritance. If a class already extends from a certain superclass, it cannot extend from Thread, and have to implement the Runnable interface. The second method is also used to provide compatibility with JDK 1.1. It should be noted that the Thread class itself implements the Runnable interface.
//    The run() method specifies the running behavior of the thread and gives the thread something to do. You do not invoke the run() method directly from your program. Instead, you create a Thread instance and invoke the start() method. The start() method, in turn, will call back the run() on a new thread.