package Multithreading.CreatingNewThreads;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by ylin183 on 1/06/2017.
 */

// Overriding run() with new thread by extending superclass Thread

//    Extend a subclass from the superclass Thread and override the run() method to specify the running behavior of the thread. Create an instance and invoke the start() method, which will call-back the run() on a new thread. For example:

public class NewThread1 extends JFrame {
    private boolean stop = false;  // start or stop the counter
    private JTextField tfCount;
    private int count = 1;

    /**
     * Constructor to setup the GUI components
     */
    public NewThread1() {
        Container cp = getContentPane();
        cp.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));
        cp.add(new JLabel("Counter"));
        tfCount = new JTextField(count + "", 10);
        tfCount.setEditable(false);
        cp.add(tfCount);

        JButton btnStart = new JButton("Start Counting");
        cp.add(btnStart);
        btnStart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                stop = false;

// Create a new Thread to do the counting

// Creating a new Thread
//    Extend a subclass from the superclass Thread and override the run() method to specify the running behavior of the thread. Create an instance and invoke the start() method, which will call-back the run() on a new thread. For example:

                Thread t = new Thread() {   // Create an instance of an anonymous inner class that extends Thread
                    @Override
                    public void run() {      // Override run() to specify the running behaviors
                        for (int i = 0; i < 100000; ++i) {
                            if (stop) break;
                            tfCount.setText(count + "");
                            ++count;
                            // Suspend itself and yield control to other threads for the specified milliseconds
                            // Also provide the necessary delay
                            try {
                                sleep(10); // milliseconds
                            } catch (InterruptedException ex) {
                            }
                        }
                    }
                };
                t.start();  // Start the thread. Call back run() in a new thread
            }
        });

        JButton btnStop = new JButton("Stop Counting");
        cp.add(btnStop);
        btnStop.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                stop = true;
            }
        });

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Counter");
        setSize(300, 120);
        setVisible(true);
    }

        /**
         * The entry main method
         */

    public static void main(String[] args) {
        // Run GUI codes in Event-Dispatching thread for thread safety
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new NewThread1();  // Let the constructor do the job
            }
        });
    }
}
