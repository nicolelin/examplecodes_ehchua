package Multithreading.SwingWorker;

import javax.swing.*;

public class SwingTemplateJApplet extends JApplet {
    /** init() to setup the GUI components */
    @Override
    public void init() {
        // Run GUI codes in the Event-Dispatching thread for thread safety
        try {
            // Use invokeAndWait() to ensure that init() exits after GUI construction
            SwingUtilities.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                    // Set the content-pane of JApplet to an instance of main JPanel
                    setContentPane(new JPanel());
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}