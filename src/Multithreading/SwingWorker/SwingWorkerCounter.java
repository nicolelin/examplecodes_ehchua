package Multithreading.SwingWorker;

import java.awt.*;
import java.awt.event.*;
import java.util.concurrent.ExecutionException;
import javax.swing.*;

/** As mentioned, in a Swing application:
 *
        Compute-intensive task should not be run on the event-dispatching thread (EDT), so as not to starve the EDT from processing events and repaints.

        Swing components shall be accessed in the EDT only for thread safety.

        The javax.swing.SwingWorkder<T,V> class helps to manage the interaction between the only EDT and several background worker threads. It can be used to schedule a compute-intensive task in a background thread and return the final result or intermediate results in the EDT.

        The signature of the SwingWorker class is as follow:
public abstract class SwingWorker<T,V> implements RunnableFuture
SwingWorker<T,V> is an abstract class with two type parameters: where T specifies the final result type of the doInBackground() and get() methods, and V specifies the type of the intermediate results of the publish() and process() methods.

        The RunnableFuture interface is the combination of two interfaces: Runnable and Future. The interface Runnable declares an abstract method run(); while Future declares get(), cancel(), isDone(), and isCancelled(). **/

/** Scheduling a Background Task

 * protected abstract T doInBackground() throws Exception
 // Do this task in a background thread

 protected void done()
 // Executes on the Event-Dispatching thread after the doInBackground() method finishes.

 public final T get() throws InterruptedException, ExecutionException
 // Waits for doInBackground() to complete and gets the result.
 // Calling get() on the Event-Dispatching thread blocks all events, including repaints,
 //  until the SwingWorker completes.

 public final void execute()
 // Schedules this SwingWorker for execution on one of the worker thread.

 public final boolean cancel(boolean mayInterruptIfRunning)
 // Attempts to cancel execution of this task.

 public final boolean isDone()
 // Returns true if this task has completed (normally or exception)

 public final boolean isCancelled()
 // Returns true if this task was cancelled before it completed normally
 **/

/** To schedule a task in a worker thread, extend a subclass of SwingWorker<T,V> (typically an inner class) and override:

        the doInBackground() to specify the task behavior, which will be scheduled in one of the worker thread and returns a result of type T.
        the done() methods, which will be run in the EDT after doInBackground() completes. In done(), use the get() method to retrieve the result of doInBackground() (of the type T). **/

/** Test SwingWorker on the counter application with a compute-intensive task */
@SuppressWarnings("serial")
public class SwingWorkerCounter extends JPanel {
    // For counter
    private JTextField tfCount;
    private int count = 0;
    // For SwingWorker
    JButton btnStartWorker;   // to start the worker
    private JLabel lblWorker; // for displaying the result

    /** Constructor to setup the GUI components */
    public SwingWorkerCounter () {
        setLayout(new FlowLayout());

        add(new JLabel("Counter"));
        tfCount = new JTextField("0", 10);
        tfCount.setEditable(false);
        add(tfCount);

        JButton btnCount = new JButton("Count");
        add(btnCount);
        btnCount.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ++count;
                tfCount.setText(count + "");
            }
        });

        /** Create a SwingWorker instance to run a compute-intensive task
         Final result is String, no intermediate result (Void) */
        final SwingWorker<String, Void> worker = new SwingWorker<String, Void>() {
            /** Schedule a compute-intensive task in a background thread */
            @Override
            protected String doInBackground() throws Exception {
                // Sum from 1 to a large n
                long sum = 0;
                for (int number = 1; number < 1000000000; ++number) {
                    sum += number;
                }
                return sum + "";
            }

            /** Run in event-dispatching thread after doInBackground() completes */
            @Override
            protected void done() {
                try {
                    // Use get() to get the result of doInBackground()
                    String result = get();
                    // Display the result in the label (run in EDT)
                    lblWorker.setText("Result is " + result);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        };

        btnStartWorker = new JButton("Start Worker");
        add(btnStartWorker);
        btnStartWorker.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                worker.execute();                 // start the worker thread
                lblWorker.setText("  Running...");
                btnStartWorker.setEnabled(false); // Each instance of SwingWorker run once
            }
        });
        lblWorker = new JLabel("  Not started...");
        add(lblWorker);

    }

    /** The entry main() method */
    public static void main(String[] args) {
        // Run the GUI construction in the Event-Dispatching thread for thread-safety
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                JFrame frame = new JFrame("SwingWorker Test");
                frame.setContentPane(new SwingWorkerCounter());
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setSize(300, 150);
                frame.setVisible(true);
            }
        });
    }
}