package Multithreading.SwingWorker;

public class Summary {

    public static void main(String[] args) {

        // http://www.ntu.edu.sg/home/ehchua/programming/java/J5e_multithreading.html
        System.out.println("Summary");
        System.out.println("Threads are essential to build a responsive graphical user interface. These are the typical situations where a new thread should be used:");
        System.out.println("To fork out a new thread for a time-consuming initialization task (such as disk I/O) in the main thread, so that the GUI comes up faster.");
        System.out.println("To fork out a new thread for a time-consuming task (within an event handler) in the event dispatch thread, so that the GUI remains responsive.");
        System.out.println("Use timer for a repetitive task, which runs at regular time interval or after a certain time delay.");
        System.out.println("To fork out a new thread if the operations need to wait for a message from another thread or program.");
        System.out.println("In addition, the compute-intensive threads must be co-operative and yield control to others.");

    }
}
